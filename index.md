Vue教學文件
==========

live-server installation
------

1. 首先需要安装Node.js
2. 打開命令列，輸入命令 `npm i live-server -g`
3. 建置專案，輸入命令 `npm init`
4. 然后輸入命令 `live-sever --port=8081` 啟動
5. 停掉 `ctrl + c`
  



Vue Basic
--------

* 參考網站：Vue 官方的API [Check Here](https://cn.vuejs.org/v2/api/#%E6%8C%87%E4%BB%A4)
* Hello World (Javascript) [Check Here](http://127.0.0.1:8081/src/basic/L1_hello_world_old.html)
* Hello World (Vue) [Check Here](http://127.0.0.1:8081/src/basic/L1_hello_world_vue.html)
* `v-if` [Check Here](http://127.0.0.1:8081/src/basic/L2_v-if.html)
* `v-show` [Check Here](http://127.0.0.1:8081/src/basic/L2_v-show.html)
* `v-model` [Check Here](http://127.0.0.1:8081/src/basic/L3_v-model.html) | [Check Here](http://127.0.0.1:8081/src/basic/L3_v-model_v2.html)
* `v-for` [Check Here](http://127.0.0.1:8081/src/basic/L4_v-for.html)
* `v-bind` [Check Here](http://127.0.0.1:8081/src/basic/L5_v-bind.html)
* `v-on` [Check Here](http://127.0.0.1:8081/src/basic/L5_v-on.html)
* `methods` [Check Here](http://127.0.0.1:8081/src/basic/L5_methods.html)
* Vue lifecycle (鉤子函式) [Check Here](http://127.0.0.1:8081/src/basic/L6_lifecycle.html)
* `v-pre, v-cloak, v-once`[Check Here](http://127.0.0.1:8081/src/basic/L6_v-pre_v-cloak_v-once.html)
* `v-html` [Check Here](http://127.0.0.1:8081/src/basic/L6_v-html.html)
	* **你的網站上動態渲染任意的HTML可能會非常危險，容易造成XSS攻擊。請只對可信內容使用HTML插值，絕不要對用戶提供的內容使用插值。**

```
<form v-on:submit.prevent="onSubmit">...</form>
v-指令:bind的參數.修飾符="Vue實體的data或方法"
```

![Exterior view](image/vue-lifecycle.png "Vue lifecycle")

> 作業一：
> 範例：Todo List [Check Here](http://127.0.0.1:8081/src/basic/hw_1/hw_1_example.html)
> 作業：[Check Here](http://127.0.0.1:8081/src/basic/hw_1/hw_1.html)
> 解答：[Check Here](http://127.0.0.1:8081/src/basic/hw_1/hw_1_answer.html)

* `computed` [Check Here](http://127.0.0.1:8081/src/basic/L7_computed.html) | [Check Here](http://127.0.0.1:8081/src/basic/L7_computed_v2.html) | [Check Here](http://127.0.0.1:8081/src/basic/L7_computed_v3.html) 
* `methods` vs `computed` [Check Here](http://127.0.0.1:8081/src/basic/L7_computed_vs_methods.html)
* `watch` [Check Here](http://127.0.0.1:8081/src/basic/L8_watch.html)
* `watch` vs `computed` [Check Here](http://127.0.0.1:8081/src/basic/L8_watch_computed.html)
* query API with lodash [Check Here](http://127.0.0.1:8081/src/basic/L8_watch_lodash.html)
* `computed`:
	* computed 不需設置初始值
	* 裡面有牽動到 data 才會跟著更換
	* computed 無法帶參數，methods 可以
* `watch`:
	* watch 需手動設置初始值
	* watch其相依的屬性有幾個，就得多幾個 watcher 監聽
	* 它的彈性讓你很容易針對資料異動設callback
* 總結:
	* `computed`: 適合處理一些數據格式，比如時間格式的轉換，字符串的截取等。
	* `watch`: 處理比較複雜的事件或是大邏輯，如允許我們執行異步操作（訪問一個API），限制我們執行該操作的頻率，並在我們得到最終結果前，設置中間狀態。 
	* `methods`: 要執行的函式，跟以上兩者無關。

> 作業二：利用Google API，查出兩地的導航距離。
> 範例：Distance between two places [Check Here](http://127.0.0.1:8081/src/basic/hw_2/hw_2_example.html)
> 作業：[Check Here](http://127.0.0.1:8081/src/basic/hw_2/hw_2.html)
> 解答：[Check Here](http://127.0.0.1:8081/src/basic/hw_2/hw_2_answer.html)
> requirements:
> 1. 所有的數字都需要四捨五入到小數點第二位，且附上單位
> 2. 輸入框要驗證
> 3. 記得送API的方法要lodash

> 作業三：匯率計算機。
> 範例：exchange rate calculator [Check Here](http://127.0.0.1:8081/src/basic/hw_3/hw_3_example.html)
> 作業：[Check Here](http://127.0.0.1:8081/src/basic/hw_3/hw_3.html)
> 解答：[Check Here](http://127.0.0.1:8081/src/basic/hw_3/hw_3_answer.html)

  

  
Vue Advanced
--------

* 複習一下Vue實體的每個屬性
```
var vm = new Vue({
	el: '#app', 		// 用來掛載Vue實體的DOM元素
	data: {},			// 要綁定的資料
	props: {},			// 用來接受外部資料的屬性
	methods: {},		// 用來定義在Vue實體內使用的方法
	watch: {},			// 用來觀察Vue實體內資料的變動
	computed: {},		// 自動為內部資料計算過的屬性
	template: "...",	// 提供Vue實體編譯後的樣板
	components: {}		// 用來定義子元件(sub-component)
});
```
* Vue template 
	* 三種 templates [Check Here](http://127.0.0.1:8081/src/advanced/L9_template.html)
	* slot [Check Here](http://127.0.0.1:8081/src/advanced/L9_slot.html)
* Vue components 
	* 全域組件與局部組件 [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_v1.html) | [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_v5.html)
	* props：從Vue實體往底下的component傳值 [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_v2.html)
	* 父子組件 [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_v3.html)
	* 組件切換 [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_v4.html)
	* 父子組件交換資料： props in, events out [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_props_in_events_out.html) | [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_props_in_events_out_v2.html)
	* 平行組件交換資料： 做一個event bus [Check Here](http://127.0.0.1:8081/src/advanced/L10_components_event_bus.html)
* Vue global API
	* `Vue.set`: 在Vue構造器外，操作內部的數據、屬性或方法 [Check Here](http://127.0.0.1:8081/src/advanced/L11_vue_set.html)
	* `Vue.directive`: 在全域上自定義新的指令 [Check Here](http://127.0.0.1:8081/src/advanced/L12_vue_directive.html)
	* 组件和指令的差別 : 
    	* 组件註冊的是一個DOM元素的標籤，而指令註冊的是已有標籤裡的一個屬性。
	* `Vue.extend`: 在全域做一個預設部分選項的實例構造器，以提供給組件使用 [Check Here](http://127.0.0.1:8081/src/advanced/L13_vue_extend.html)
		
		![Exterior view](image/vue-vs-vue_component.jpg "Vue vs Vue component")

	* `propsData` 屬性: 在全域傳遞資料到實例構造器裡 [Check Here](http://127.0.0.1:8081/src/advanced/L14_propsData.html)
	* `Vue.mixin`: 用來寫共用方法時使用 [Check Here](http://127.0.0.1:8081/src/advanced/L15_vue_mixin.html)
	* 當多數組件中有共同方法及物件是會被經常使用到的時候，希望抽出成獨立的方法讓所有組件重複使用的三種做法  [Check Here](http://127.0.0.1:8081/src/advanced/L15_vue_mixin_v2.html)。
		1. `Vue.prototype`: 與 Vue 組件較無相依性的方法 / 物件
		2. `mixins`: 少部分組件會使用到的共用方法
		3. `global mixin`: 全部組件會使用到的共用方法
	* `mixins` vs `extends` 屬性：mixins接受物件陣列（多重繼承），extends接受物件或方法（單一繼承）[Check Here](http://127.0.0.1:8081/src/advanced/L15_vue_mixins_vs_extends.html)
* 一些Vue的實例屬性與方法
	* 屬性： [$data, $refs](http://127.0.0.1:8081/src/advanced/L16_vue_instance_options.html), $parent
	* 方法： [$mount, $destroy, $forceUpdate](http://127.0.0.1:8081/src/advanced/L16_vue_instance_methods.html),  [$nextTick](http://127.0.0.1:8081/src/advanced/L16_vue_instance_methods_nextTick.html), $emit, $on, [$off](http://127.0.0.1:8081/src/advanced/L10_components_props_in_events_out_v3.html)
	* Vue是`異步執行DOM更新`的 (Vue響應式原理)
		* Vue在修改數據後，DOM不會立刻更新，而是等同一個事件循環中所有數據發生變化完成之後，再統一進行DOM的更新。
	* Javascript 主線程與任務隊列示意圖：[參考資料](http://www.ruanyifeng.com/blog/2014/10/event-loop.html)

	![Exterior view](image/javascript-event-loop.png "Javascript 主線程與任務隊列示意圖")
	![Exterior view](image/vue-nextTick.png "Vue nextTick")

	* 總結：
		1. 同步代碼執行 -> 查找異步隊列，推入執行線，然後才執行Vue.nextTick的回調函式[事件循環1] -> 查找異步隊列，推入執行線，然後才執行Vue.nextTick的回調函式[事件循環2]...
		2. 異步是一個單獨的Tick，不會在同步在同一個Tick發生。ex. click事件的回調函式也是一個異步。

	* `nextTick`使用時機：
		1. `created()`進行DOM元素的操作時：因為Vue還未對任何的DOM元素進行渲染，所以一定要將進行DOM元素的操作程式碼放入`Vue.nextTick()`的回調函式裡。
		2. 在數據變化後要進行某項操作，而這項操作會隨著數據改變而改變DOM的結構時，此項操作應放在`Vue.nextTick()`的回調函式裡。
* 補充：Vue file [Check Here](http://127.0.0.1:8081/src/advanced/L17_vue_file.html)	
* 安裝Chrome plugin: Vue調試工具
* https://pjchender.blogspot.com/2017/05/vue-vue-reactivity.html

> 作業四：動態搜尋Blogs文章
> 範例：Blogs [Check Here](http://127.0.0.1:8081/src/advanced/hw_1/hw_1_example.html)
> 作業：[Check Here](http://127.0.0.1:8081/src/advanced/hw_1/hw_1.html)
> 解答：[Check Here](http://127.0.0.1:8081/src/advanced/hw_1/hw_1_answer.html)

> 作業五：動態房間管理系統
> 範例：Rooms [Check Here](http://127.0.0.1:8081/src/advanced/hw_2/hw_2_example.html)
> 作業：[Check Here](http://127.0.0.1:8081/src/advanced/hw_2/hw_2.html)
> 解答：[Check Here](http://127.0.0.1:8081/src/advanced/hw_2/hw_2_answer.html)

> 作業六：共用方法應用
> 範例：寫一支擁有i18n的網頁 [Check Here](http://127.0.0.1:8081/src/advanced/hw_3/hw_3_example.html)
> 說明： 1. 讀取語系檔 2. 讀取完畢後存入localStorage 3. 寫一些可以轉換語系的共用方法（`Vue.mixin`） 4. 在每個要轉換語系的標籤上註冊自定義的指令（`Vue.directive`） 5. 頁面轉換請用子組件切換
> 作業：[Check Here](http://127.0.0.1:8081/src/advanced/hw_3/hw_3.html)
> 解答：[Check Here](http://127.0.0.1:8081/src/advanced/hw_3/hw_3_answer.html)
  




vue-cli
-------------

##### vue-cli installation
1. 首先需要安装vue-cli，打開命令列，輸入命令 `npm install vue-cli -g`
2. 確認vue-cli版本號，輸入命令 `vue -V`
3. 建置目錄，輸入命令 `mkdir vuecliTest`
4. 建置專案，輸入命令 `vue init webpack vuecliTest`
	* `vue init <template-name> <project-name>`
	* vue-cli官方提供五種模版：
		* `webpack`: 一个全面的webpack+vue-loader的模板，功能包括热加载，linting,检测和CSS扩展。
		* `webpack-simple`: 一个简单webpack+vue-loader的模板，不包含其他功能，让你快速的搭建vue的开发环境。
		* `browserify`: 一个全面的Browserify+vueify 的模板，功能包括热加载，linting,单元检测。
		* `browserify-simple`: 一个简单Browserify+vueify的模板，不包含其他功能，让你快速的搭建vue的开发环境。
		* `simple`: 一个最简单的单页应用模板。
	* 會問幾個關鍵問題：
	* Install vue-router? `Y`
	* Use ESLint to lint your code? `n`
	* setup unit tests with Karma + Mocha? `n`
	* Setup e2e tests with Nightwatch? `n`
5. 輸入命令 `cd vuecliTest` 
6. 安裝package.json裡的項目依賴包，輸入命令 `npm install`
7. 啟動 `npm run dev`
8. 停掉 `ctrl + c`


##### vue-cli專案目錄結構
<pre style="background-color:#ddd; padding: 0px 50px;">
.
|-- build                       // 項目構建(webpack)相關代碼
|   |-- build.js                // 生產環境構建代碼，npm run build會執行此檔案
|   |-- check-version.js        // 檢查node、npm等版本
|   |-- vue-loader.conf.js      // 
|   |-- utils.js                // 構建工具相關
|   |-- webpack.base.conf.js    // webpack基礎配置，npm run dev會執行此檔案
|   |-- webpack.dev.conf.js     // webpack開發環境配置
|   |-- webpack.prod.conf.js    // webpack生產環境配置
|-- config                      // 項目開發環境配置
|   |-- dev.env.js              // 開發環境變量
|   |-- index.js                // 項目一些配置變量
|   |-- prod.env.js             // 生產環境變量
|-- dist                        // npm run build完產生的專案打包檔
|-- node_modules                // 第三方依賴包存放的位置
|-- src                         // 源碼目錄
|   |-- assets                  // 
|   |-- components              // vue公共组件
|   |-- store                   // vuex的狀態管理
|   |-- router                  // vue-router的路由管理
|   |-- App.vue                 // 頁面入口文件
|   |-- main.js                 // 程式入口文件，加載各種公共組件
|-- static                      // 靜態文件，比如一些圖片，json數據等
|   |-- data                    // 群聊分析得到的數據用於數據可視化
|-- .babelrc                    // ES6语法編譯配置
|-- .editorconfig               // 定義代碼格式
|-- .gitignore                  // git上傳需要忽略的文件格式
|-- README.md                   // 項目说明
|-- favicon.ico 
|-- index.html                  // 入口頁面
|-- package.json                // 項目基本資訊，npm install {package} --save安裝	
.
</pre>
  




vue-router
-------------

##### 簡單說明
* vue-router是一種 SPA（單頁應用）的路徑管理器，也就是說，整個專案只有一個index.html的頁面，不再透過`<a></a>`做鍵聯，改用`vue-router`進行管理。

##### 增加一個Hi頁面與路由
1. 在`src/components/`目錄下，新建`Hi.vue`
	```
	<template>
		<div>
			<h1>{{ msg }}</h1>
		</div>
	</template>
	<script>
		export default {
		name: 'hi',
		data () {
				return {
					msg: 'Hi, I am Fabian'
				}
			}
		}
	</script>

	```
2. 在`src/router/index.js`進行配置路由
	```javascript
	import Vue from 'vue'   //引入Vue
	import Router from 'vue-router'  //引入vue-router
	import Hello from '@/components/Hello'  //引入根目錄下的Hello.vue组件
	import Hi from '@/components/Hi'
	Vue.use(Router)  //Vue全局使用Router

	export default new Router({
	routes: [                  //配置路由，这里是陣列
		{                      //每一個鍵接都是一個物件
			path: '/',         //鍵接路徑
			name: 'Hello',     //路由名稱
			component: Hello   //對應的组件模板
		},
		{                      //每一個鍵接都是一個物件
			path: '/hi',       //鍵接路徑
			name: 'Hi',        //路由名稱
			component: Hello   //對應的组件模板
		}
	]
	});
	```
3. 在`src/App.vue`下用`<router-link to=""></router-link>`做導航
	```
	<template>
		<div id="app">
			<img src="./assets/logo.png">
			<div>
				<router-link to="/">首頁</router-link> ｜
				<router-link to="/hi">hi頁面</router-link> ｜
			</div>
			<p>{{$route.name}}</p>
			<router-view/>
		</div>
	</template>

	<script>
	export default {
	name: 'App'
	}
	</script>
	```

##### 子路由
1. `src/App.vue`
	```
	<template>
		<div id="app">
			<img src="./assets/logo.png">
			<div>
			<router-link to="/">首頁</router-link> ｜
			<router-link to="/hi">hi頁面</router-link> ｜
			<router-link :to="/hi/hi1">hi頁面1</router-link> ｜
			<router-link to="/hi/hi2">hi頁面2</router-link> ｜
			</div>
			<p>{{$route.name}}</p>
			<router-view/>
		</div>
	</template>

	<script>
		export default {
			name: 'App'
		}
	</script>

	<style>
		#app {
			font-family: 'Avenir', Helvetica, Arial, sans-serif;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
			text-align: center;
			color: #2c3e50;
			margin-top: 60px;
		}
	</style>

	```
2. `src/router/index.js`路由配置
	```javascript
	import Vue from 'vue'
	import Router from 'vue-router'
	import HelloWorld from '@/components/HelloWorld'
	import Hi from '@/components/Hi'
	import Hi1 from '@/components/Hi1'
	import Hi2 from '@/components/Hi2'

	Vue.use(Router)

	export default new Router({
		routes: [
			{
				path: '/',
				name: 'HelloWorld',
				component: HelloWorld
			},
			{
				path: '/hi',
				component: Hi,
				children: [
					{path: '/', name: 'hi', component: Hi},
					{path: 'hi1', name: 'hi1', component: Hi1},
					{path: 'hi2', name: 'hi2', component: Hi2},
				]
			},
		]
	});

	```
3. 在`src/components/`目錄下，新建`Hi.vue`，`Hi1.vue`和`Hi2.vue`
	`Hi.vue`
	```
	<template>
		<div>
			<h2>{{msg}}</h2>
			<router-view/>
		</div>
	</template>

	<script>
		export default {
			name: 'Hi',
			data () {
				return {
				msg: 'I am Hi page!'
				}
			}
		}
	</script>

	<style scoped>
	</style>
	```
	`Hi1.vue`
	```
	<template>
		<h2>{{msg}}</h2>
	</template>

	<script>
		export default {
			name: 'Hi1',
			data () {
				return {
				msg: 'I am Hi1 page!'
				}
			}
		}
	</script>

	<style scoped>
	</style>
	```
	`Hi2.vue`
	```
	<template>
		<h2>{{msg}}</h2>
	</template>

	<script>
		export default {
			name: 'Hi2',
			data () {
				return {
				msg: 'I am Hi2 page!'
				}
			}
		}
	</script>

	<style scoped>
	</style>
	```

##### 單頁面多路由區域操作
1. `src/App.vue`
	```
	<template>
		<div id="app">
			<img src="./assets/logo.png">
			<router-view/>
			<router-view name="left" id="left"/>
			<router-view name="right" id="right"/>
		</div>
	</template>

	<script>
		export default {
			name: 'App'
		}
	</script>

	<style>
		#app {
			font-family: 'Avenir', Helvetica, Arial, sans-serif;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
			text-align: center;
			color: #2c3e50;
			margin-top: 60px;
		}
		#left{
			float: left;
			width: 50%;
			height: 300px;
			background-color: #feae13;
		}
		#right{
			float: left;
			width: 50%;
			height: 300px;
			background-color: #abcabc;
		}
	</style>

	```
2. `src/router/index.js`路由配置
	```javascript
	import Vue from 'vue'
	import Router from 'vue-router'
	import HelloWorld from '@/components/HelloWorld'
	import Hi1 from '@/components/Hi1'
	import Hi2 from '@/components/Hi2'

	Vue.use(Router)

	export default new Router({
	routes: [
		{
			path: '/',
			name: 'HelloWorld',
			components: {
				default: HelloWorld,
				left: Hi1,
				right: Hi2
			}
		},
		{
			path: '/test',
			name: 'HelloWorld2',
			components: {
				default: HelloWorld,
				left: Hi2,
				right: Hi1
			}
		}
	]
	});
	```
3. 在`src/components/`目錄下，新建`Hi1.vue`和`Hi2.vue`
	```
	<template>
		<h1>{{msg}}</h1>
	</template>
	<script>
	export default {
		data(){
			return {
				msg: "I'm a Hi1"
			}
		}
	}
	</script>
	```

##### 傳遞參數、重導、404畫面、編程式導航
1. `src/App.vue`
	```
	<template>
		<div id="app">
			<img src="./assets/logo.png"><br>
			<button @click="goBack">Back</button>
			<button @click="goFront">Front</button>
			<button @click="goHome">Home</button>
			<br>
			<router-link to="/">Home</router-link> |
			<router-link to="/goHome">goHome</router-link> |
			<router-link :to="{ name: 'Params', params: {newsTitle: 'Fabian', newsId: '888'} }">標籤to傳遞參數</router-link> ｜
			<router-link to="/params/198/Fabian Website">url傳遞參數</router-link> |
			<router-link to="/params/222/I like vue.js">goParams</router-link> |
			<router-link to="/hi1">Hi1</router-link> |
			<router-link to="/fabian">Fabian</router-link> |
			<router-link to="/xfefwefw">我是瞎寫的</router-link> |

    		<!-- mode="out-in": 當前元素先進行過渡離開，離開完成後新元素過渡進入 -->
			<transition name="fade" mode="out-in">
				<router-view/>
			</transition>
		</div>
	</template>

	<script>
		export default {
			name: 'App',
			methods: {
				goBack(){
					this.$router.go(-1);
				},
				goFront(){
					this.$router.go(1);
				},
				goHome(){
					this.$router.push('/');
				}
			}
		}
	</script>

	<style>
		#app {
			font-family: 'Avenir', Helvetica, Arial, sans-serif;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
			text-align: center;
			color: #2c3e50;
			margin-top: 60px;
		}
		.fade-enter {
			opacity:0;
		}
		.fade-leave{
			opacity:1;
		}
		.fade-enter-active{
			transition:opacity .5s;
		}
		.fade-leave-active{
			opacity:0;
			transition:opacity .5s;
		}
	</style>
	```
2. `src/router/index.js`路由配置
	```javascript
	import Vue from 'vue'
	import Router from 'vue-router'
	import HelloWorld from '@/components/HelloWorld'
	import Params from '@/components/Params'
	import Hi1 from '@/components/Hi1'
	import Error from '@/components/Error'
	Vue.use(Router)

	export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'HelloWorld',
			component: HelloWorld
		},
		{
			path: '/params/:newsId(\\d+)/:newsTitle',
			name: 'Params',
			component: Params,
			beforeEnter: (to, from, next) => {
				console.log(to);
				console.log(from);
				next(); // 允許往下跳轉
				// next({path: '/'}); //更改跳轉路由
			}
		}, 
		{
			path: '/goHome',
			redirect: '/'
		},
		{
			path: '/params/:newsId(\\d+)/:newsTitle',
			redirect: '/params/:newsId(\\d+)/:newsTitle' // 重導要傳遞參數時
		}, 
		{
			path: '/hi1',
			name: 'Hi1',
			component: Hi1,
			alias: '/fabian'// 保留訪問路徑，並且<route-view>的内容指向到'/hi'
		}, 
		{
			path: '*',
			component: Error,
		}, 
	]
	})
	```
3. 在`src/components/`目錄下，新建`Params.vue`和`Error.vue`
	```
	<template>
		<div>
			<h1>{{msg}}</h1>
			<p>news Id: {{ $route.params.newsId }}</p>
			<p>news Title: {{ $route.params.newsTitle }}</p>
		</div>
	</template>
	<script>
		export default {
			data(){
				return {
					msg: "Params page"
				}
			},
			beforeRouteEnter: (to, from, next) =>{
				console.log('before enter the params page');
				next();
			},
			beforeRouteLeave: (to, from, next) =>{
				console.log('ready to leave the params page');
				next();
			},
		}
	</script>

	```
	```
	<template>
		<div>
			<h1>{{msg}}</h1>
			Please type the correct url.
		</div>
	</template>
	<script>
	export default {
		data(){
			return {
				msg: "Error [404]: page not found!"
			}
		}
	}
	</script>
	```

> 作業七：利用vue-cli與vue-router建構一個sb-admin後台系統
> 範例： `git clone git@gitlab.com:fabian415/sb-admin.git`
> 解答： `git clone git@gitlab.com:fabian415/sb-admin-vue.git`

vue-cli在實務上可能遇到的問題
-------------

* 如何在Vue-cli裡全域載入jQuery? 
	1. 執行命令：`npm install -save jquery popper.js`
	2. 在`build/webpack.dev.conf.js`底下
	```javascript
	const devWebpackConfig = merge(baseWebpackConfig, {
		plugins: [
			// 這裡註冊jquery plugin
			new webpack.ProvidePlugin({
				$: "jquery",
				jQuery: "jquery"
			}),
		]
	}
	```
* 如何更改根目錄(`src`)的別名?
	1. 在`build/webpack.base.conf.js`底下
	```javascript
	module.exports = {
		context: path.resolve(__dirname, '../'),
		entry: { // Vue程式進入點
			app: './src/main.js'
		},
		output: { // 打包成什麼檔
			path: config.build.assetsRoot,
			filename: '[name].js',
			publicPath: process.env.NODE_ENV === 'production'
			? config.build.assetsPublicPath
			: config.dev.assetsPublicPath
		},
		resolve: {
			extensions: ['.js', '.vue', '.json'],
			alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@': resolve('src'), // 這裡可以改`src`的別名
			}
		},
	}
	```
* 如何在Vue-cli裡全域載入bootstrap或其他套件或CSS?  
	1. 執行命令：`npm install -save bootstrap@4.1.3`, `npm install -save @fortawesome/fontawesome-free`
	2. 在`src/App.vue`底下
	```javascript
	import 'bootstrap'; // 載入bootstrap.js
	import 'bootstrap/dist/css/bootstrap.css'; // 載入bootstrap.css
	import '@fortawesome/fontawesome-free/css/all.min.css'; // 載入fontawesome.css
	import 'jquery-easing';  // 其它套件

	export default {
		name: 'App'
	}
	```


Vuex
--------

* 共用狀態管理器
* `Vue` + flu`x` = `vuex`
* Why use `vuex` ? 一個大型系統中，重複使用到的資料一定很多，這樣各個頁面切換之間如何拿到同一筆資料呢？ ex: token, user info, shopping cart list etc..
* What is `vuex` ?
	* 集中式儲存管理應用的所有组件的狀態
	* 單向資料流
	* Flux design pettern
	* 官方推薦項目之一
![Exterior view](image/vuex.jpg "Vuex demo")

* 注意：
	* 起始點都是action，並且是單向資料流，vue -> dispatch -> action -> commit -> mutation -> state changed -> vue
	* 不管是api還是直接改狀態，都要在action新增方法，維持框架單向的流程

* login流程為例：

步驟 | 流程描述 | Flux(vuex) 流程
---- | --- | ---
1 | 在登入頁按下 login 按鈕 | vue
2 | 按鈕觸發 action | action
3 | action 調用 login API | action
4 | server response success 帳號密碼正確 | action(`commit`)
5 | 接收 action 資料，計算邏輯，改變狀態 | mutation
6 | State 改變 ex: `login: true or token: '3345678'` | state
7 | 轉跳到 index 頁面 | vue

* 執行指令進行安裝：`npm install vuex --save`

##### 最簡單的範例 （ref: /Users/fabian/classDemo/vuex/simpleCounter）
1. 在`src`目錄下，新建`store/index.js`
	```javascript
	import Vue from 'vue'
	import Vuex from 'vuex' // import vuex
	Vue.use(Vuex)

	const state = {
		count: 1
	}
	const mutations = {
		add(state) {
			state.count++
		},
		reduce(state) {
			state.count--
		}
	}

	export default new Vuex.Store({
		state,
		mutations
	})

	```
2. 在`src`目錄下，在`main.js`全局引入`store/index.js`，並注入到`Vue`實體
	```javascript
	import Vue from 'vue'
	import App from './App'
	import router from './router'
	import store from '@/store' // 預設會去找store底下的index.js
	// import store from '@/store/index'

	Vue.config.productionTip = false

	/* eslint-disable no-new */
	new Vue({
		el: '#app',
		router, 
		store, // 全域引入後，每一個component都有store物件，可用$store取得
		components: { App },
		template: '<App/>'
	})
	```

3. 在`src/components/`目錄下，新建`Counter.vue`
	```
	<template>
		<div>
			<h2>{{msg}}</h2>
			<hr/>
			<h3>{{$store.state.count}}</h3>
			<p>
				<button @click="$store.commit('add')">+</button>
				<button @click="$store.commit('reduce')">-</button>
			</p>
		</div>
	</template>

	<script>
		export default {
			data(){
				return {
					msg:'Hello Vuex',
				}
			}
		}
	</script>
	```

##### state狀態物件 - 如何在元件中取得vuex的state
1. 在`src/components/`目錄下，新建`Counter2.vue`
	```
	<template>
		<div>
			<h2>{{msg}}</h2>
			<hr/>
			<h3>{{$store.state.count}} - {{count}}</h3>
			<p>
				<button @click="$store.commit('add')">+</button>
				<button @click="$store.commit('reduce')">-</button>
			</p>
		</div>
	</template>

	<script>
		import { mapState } from 'vuex'
		export default {
			data(){
				return {
					msg:'Hello Vuex',
				}
			},
			// computed: { // 第一種
			//     count () {
			//         return this.$store.state.count
			//     }
			// },
			// computed: mapState({ // 第二種
			//     count(state){
			//         return state.count
			//     }
			// }),
			computed: mapState(['count']) // 第三種：建議使用
		}
	</script>
	```

##### mutations 同步改變狀態的方法 - 傳值、如何在元件中取得vuex的mutations
1. 修改`src/store/index.js`
	```javascript
	import Vue from 'vue'
	import Vuex from 'vuex' // import vuex
	Vue.use(Vuex)

	const state = {
		count: 1
	}
	const mutations = {
		add(state, n) { // 第一個參數固定為state
			if(typeof n === "undefined") n = 1
			state.count += n
		},
		reduce(state) {
			state.count--
		}
	}

	export default new Vuex.Store({
		state,
		mutations
	})
	```

2. 在`src/components/`目錄下，新建`Counter3.vue`
	```
	<template>
		<div>
			<h2>{{msg}}</h2>
			<hr/>
			<h3>{{count}}</h3>
			<p>
				<!-- 第一個參數固定為方法名稱 -->
				<!-- <button @click="$store.commit('add', 10)">+</button>
				<button @click="$store.commit('reduce')">-</button> -->
				<!-- 建議使用 -->
				<button @click="add(10)">+</button>
				<button @click="reduce">-</button>
			</p>
		</div>
	</template>

	<script>
		import { mapState, mapMutations } from 'vuex'
		export default {
			data(){
				return {
					msg:'Hello Vuex',
				}
			},
			computed: mapState(['count']), // 第三種：建議使用
			methods: mapMutations(['add', 'reduce'])  // 建議使用
		}
	</script>
	```

##### getters - 數據的過濾、加工與計算、如何在元件中使用vuex的getters
1. 修改`src/store/index.js`
	```javascript
	import Vue from 'vue'
	import Vuex from 'vuex' // import vuex
	Vue.use(Vuex)

	const state = {
		count: 1
	}
	const mutations = {
		add(state, n) { // 第一個參數固定為state
			if(typeof n === "undefined") n = 1
			state.count += n
		},
		reduce(state) {
			state.count--
		}
	}
	const getters = {
		count (state){ // state裡的變數 (參數固定為state) {}
			return state.count += 100
		}
	}

	export default new Vuex.Store({
		state,
		mutations,
		getters
	})
	```

2. 在`src/components/`目錄下，新建`Counter4.vue`
	```
	<template>
		<div>
			<h2>{{msg}}</h2>
			<hr/>
			<h3>{{count}}</h3>
			<p>
				<button @click="add(10)">+</button>
				<button @click="reduce">-</button>
			</p>
		</div>
	</template>

	<script>
		import { mapState, mapMutations, mapGetters } from 'vuex'
		export default {
			data(){
				return {
					msg:'Hello Vuex',
				}
			},
			computed: {
				...mapState(['count']), // 未使用getters取狀態。 ... 為ES6擴展運算符
				// count () { // 第一種Getters
				//     return this.$store.getters.count
				// },
				...mapGetters(['count']), // 第二種Getters，建議使用
			},
			methods: mapMutations(['add', 'reduce'])  // 建議使用
		}
	</script>
	```

##### actions 異步改變狀態的方法
1. 修改`src/store/index.js`
	```javascript
	import Vue from 'vue'
	import Vuex from 'vuex' // import vuex
	Vue.use(Vuex)

	const state = {
		count: 1
	}
	const mutations = {
		add(state, n) { // 第一個參數固定為state
			if(typeof n === "undefined") n = 1
			state.count += n
		},
		reduce(state) {
			state.count--
		}
	}
	const getters = {
		count (state){ // state裡的變數 (參數固定為state) {}
			return state.count += 100
		}
	}
	const actions = {
		addAction (context) { // 預設帶context上下文，這裡指的是Vue.Store物件本身
			context.commit('add', 10) // 執行Vue.Store物件裡的mutations方法，要用commit
			
			setTimeout(function(){ // 異步的範例，因為有context物件，可以在異步裡執行mutations方法
				context.commit('reduce')
			}, 1000)
		},
		reduceAction ( { commit } ) { // 直接引用本文（context）裡的commit方法，並以一個物件包裝起來
			commit('reduce')
		}
	}

	export default new Vuex.Store({
		state,
		mutations,
		getters,
		actions
	})
	```

2. 在`src/components/`目錄下，新建`Counter5.vue`
	```
	<template>
		<div>
			<h2>{{msg}}</h2>
			<hr/>
			<h3>{{count}}</h3>
			<p>
				<!-- <button @click="add(10)">+</button>
				<button @click="reduce">-</button> -->
				<button @click="addAction">+</button>
				<button @click="reduceAction">-</button>
			</p>
		</div>
	</template>

	<script>
		import { mapState, mapMutations, mapGetters, mapActions } from 'vuex'
		export default {
			data(){
				return {
					msg:'Hello Vuex',
				}
			},
			computed: {
				...mapState(['count']), // 未使用getters取狀態。 ... 為ES6擴展運算符
			},
			methods: {
				// ...mapMutations(['add', 'reduce']),  // 建議使用
				...mapActions(['addAction', 'reduceAction'])
			}
		}
	</script>
	```

##### modules - (大型專案) 對不同功能的共享狀態進行分類 （ref: /Users/fabian/classDemo/vuex/moduleCounter）
1. 在`store/module`目錄下，新建`user.js`，將`state, mutations, getters, actions`放入
	```javascript
	const state = {
		count: 1
	}
	const mutations = {
		add(state, n) { // 第一個參數固定為state
			if(typeof n === "undefined") n = 1
			state.count += n
		},
		reduce(state) {
			state.count--
		}
	}
	const getters = {
		count (state){ // state裡的變數 (參數固定為state) {}
			return state.count += 100
		}
	}
	const actions = {
		addAction (context) { // 預設帶context上下文，這裡指的是Vue.Store物件本身
			context.commit('add', 10) // 執行Vue.Store物件裡的mutations方法，要用commit
			
			setTimeout(function(){ // 異步的範例，因為有context物件，可以在異步裡執行mutations方法
				context.commit('reduce')
			}, 1000)
		},
		reduceAction ( { commit } ) { // 直接引用本文（context）裡的commit方法，並以一個物件包裝起來
			commit('reduce')
		}
	}

	export default {
		namespaced: true, // 如果我們希望模塊可以有更高的封裝度與重用性，我們可以通過新增 namespaced: true 的方式使其成為命名空間模塊。當模塊被註冊後，它所有的 getter 、 action 與 mutation 都會自動根據模塊註冊的路徑調整命名。
		state,
		mutations,
		getters,
		actions
	}
	```

2. 調整`store/index.js`
	```javascript
	import Vue from 'vue'
	import Vuex from 'vuex' // import vuex
	Vue.use(Vuex)
	import user from './module/user'

	export default new Vuex.Store({
		modules: {
			user: user
		}
	})
	```

3. . 在`src`目錄下，在`main.js`全局引入`store/index.js`，並注入到`Vue`實體
	```javascript
	import Vue from 'vue'
	import App from './App'
	import router from './router'
	import store from '@/store' // 預設會去找store底下的index.js
	// import store from '@/store/index'

	Vue.config.productionTip = false

	/* eslint-disable no-new */
	new Vue({
		el: '#app',
		router, 
		store, // 全域引入後，每一個component都有store物件，可用$store取得
		components: { App },
		template: '<App/>'
	})
	```

4. 在`src/components/`目錄下，調整`Counter5.vue`
	```
	<template>
		<div>
			<h2>{{msg}}</h2>
			<hr/>
			<h3>{{count}}</h3>
			<p>
				<button @click="addAction">+</button>
				<button @click="reduceAction">-</button>
			</p>
		</div>
	</template>

	<script>
		// 建立命名空間 helpers
		import { createNamespacedHelpers } from "vuex"
		const { mapState, mapMutations, mapGetters, mapActions } = createNamespacedHelpers("user")

		export default {
			data(){
				return {
					msg:'Hello Vuex',
				}
			},
			computed: {
				...mapState(['count']), // 未使用getters取狀態。 ... 為ES6擴展運算符
			},
			methods: {
				// ...mapMutations(['add', 'reduce']),  // 建議使用
				...mapActions(['addAction', 'reduceAction'])
			}
		}
	</script>
	```

> 作業八：對actions, mutations, mutation_types, getters, index分五個不同的js檔案，並且分在user的module裡 (超大型專案)
> 解答： `/Users/fabian/classDemo/vuex/complexCounter`

