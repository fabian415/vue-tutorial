MongoDB教學文件
==========

安裝 (mac)
------

1. 利用Homebrew安装Redis，輸入命令 `sudo brew install mongodb`
2. `sudo mkdir -p /data/db`
2. Server端：啟動MongoDB，輸入指令`sudo mongod`，啟動後監聽`27017` port
3. client端：確認是否可以連？另外打開terminal，輸入指令`cd /usr/local/bin`，輸入指令`./mongo`，輸入指令`show dbs`
4. 關閉MongoDB，輸入命令 `ctrl + c`
  http://www.runoob.com/mongodb/mongodb-tutorial.html