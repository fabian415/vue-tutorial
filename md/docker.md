Docker教學文件
==========

安裝 (mac/windows)
------

1. 官網下載[docker desktop](https://www.docker.com/products/docker-desktop)
2. 安装成功後，輸入命令 `docker --version`，`docker-compose --version`，`docker-machine --version`
3. 安裝Docker GUI管理介面 [Kitematic](https://kitematic.com/)

docker 基礎概念
------

* Docker 是一個開源專案，出現於 2013 年初，最初是 Dotcloud 公司內部的 Side-Project。
* 它基於 Google 公司推出的 Go 語言實作。（ Dotcloud 公司後來改名為 Docker ）
* Comparing Containers and Virtual Machines ( 傳統的虛擬化 )
![Exterior view](image/docker.png "Docker vs VM")
* Image: 映像檔，可以把它想成是以前我們在玩 VM 的 Guest OS（ 安裝在虛擬機上的作業系統 ）。唯讀（ R\O ）的。
* Container: 容器，利用映像檔（ Image ）所創造出來的，一個 Image 可以創造出多個不同的 Container，Container 也可以被啟動、開始、停止、刪除，並且互相分離。 讀寫模式（ R\W ）。
* Registry: 可以把它想成類似 GitHub，裡面存放了非常多的 Image ，可在 [Docker Hub](https://hub.docker.com/) 中查看。


docker 基礎指令
------

1. Test it! `docker run hello-world`
2. 查看目前的 image `docker images`
3. 刪除 image `docker rmi CONTAINER [CONTAINER...]`
4. 查看目前運行的 container `docker ps`
5. 查看目前全部的 container（ 包含停止狀態的 container ） `docker ps -a`
6. 啟動 container `docker start [OPTIONS] CONTAINER [CONTAINER...]`
7. 停止 container `docker stop [OPTIONS] CONTAINER [CONTAINER...]`
8. 重新啟動 container `docker restart [OPTIONS] CONTAINER [CONTAINER...]`
9. 刪除 container `docker rm [OPTIONS] CONTAINER [CONTAINER...]`
10. 進入 container `docker exec -it <Container ID> bash` 或者使用 Kitemetic，點選 exec
11. 查看 Container 詳細資料 `docker inspect [OPTIONS] NAME|ID [NAME|ID...]`
12. 查看 log `docker logs [OPTIONS] CONTAINER` options: --follow , -f , Follow log output
13. 停止指定的 CONTAINER 中全部的 processes `docker pause CONTAINER [CONTAINER...]`
14. 恢復指定暫停的 CONTAINER 中全部的 processes `docker unpause CONTAINER [CONTAINER...]`
15. 建立一個名稱為 busybox 的 image (-t: Allocate a pseudo-TTY): `docker create -it --name busybox busybox`
16. 新建並啟動container `docker run --detach --publish=80:80 --name=webserver nginx` -d 代表在 Detached（ 背景 ）執行，如不加 -d，預設會 foreground ( 前景 ) 執行; -p 代表將本機的 80 port 的所有流量轉發到 container 中的 80 port; --name 設定 container 的名稱
17. 新建並啟動container `docker run -it --rm busybox` --rm 代表當 exit container 時，會自動移除 container。 ( incompatible with -d )
18. 儲存 (備份) image 成 tar 檔案 `docker save --output busybox.tar busybox`
19. 載入 image `docker load -i busybox.tar`

20. 停止所有正在運行的 Container `docker container stop $(docker ps -q)`
21. 移除全部停止的 containers `docker container prune`

`docker run -p 6379:6379 --name my-redis -v /Users/fabian/redis/redis.conf:/etc/redis/redis.conf -v /Users/fabian/redis/data:/data -d redis redis-server /etc/redis/redis.conf --appendonly yes`


docker-compose 基本概念
------

* Compose 是定義和執行多 Container 管理的工具
* 通常一個 Web 都還會有 DB，甚至可能還有 Redis 或 Celery，所以說我們需要有 Compose 來管理這些，透過 `docker-compose.yml` ( YML 格式 ) 文件。


docker-compose 基礎指令
------

1. 查看目前 Container `docker-compose ps` 加上 `-q` 的話，只顯示 id
2. 啟動 Service 的 Container `docker-compose start  [SERVICE...]`
3. 停止 Service 的 Container ( 不會刪除 Container ) `docker-compose stop [options] [SERVICE...]`
4. 重啟 Service 的 Container `docker-compose restart [options] [SERVICE...]`
5. Builds, (re)creates, starts, and attaches to containers for a service `docker-compose up [options] [--scale SERVICE=NUM...] [SERVICE...]` 加個 `-d`，會在背景啟動，一般建議正式環境下使用。
6. 




