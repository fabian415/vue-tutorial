Redis教學文件
==========

安裝 (mac)
------

1. 利用Homebrew安装Redis，輸入命令 `brew install redis`
2. Server端：啟動redis，輸入指令`redis-server /usr/local/etc/redis.conf`，啟動後監聽`6379` port
3. client端：確認是否可以連？另外打開terminal，輸入指令`redis-cli`，輸入指令`ping`，若得到`PONG`字樣，代表連線成功
4. 關閉redis，輸入命令 `ctrl + c`
5. 設置密碼，打開`/usr/local/etc/redis.conf`檔案，打開`requirepass 123456`的註解，後面`123456`則是密碼設置區
6. 若是以client去連，需做一次密碼驗證，輸入指令`auth 123456`
  

安裝 (windows)
------

1. [下載地址](https://github.com/MSOpenTech/redis/releases)
2. Server端：啟動redis，輸入指令`redis-server.exe redis.windows.conf`，啟動後監聽`6379` port
3. client端：確認是否可以連？另外打開terminal，輸入指令`redis-cli.exe -h 127.0.0.1 -p 6379`，輸入指令`ping`，若得到`PONG`字樣，代表連線成功
4. 關閉redis，輸入命令 `ctrl + c`
  
簡介
--------

* 參考網站：Redis教程 [Check Here](http://www.runoob.com/redis/redis-tutorial.html)
* `RE`mote `DI`ctionary `S`erver (`Redis`) 是一個由Salvatore Sanfilippo寫的key-value儲存系统
* 支持`數據持久化`，可以將記憶體中的數據保存在硬碟中，重啟的時候可以再次加載至記憶體進行使用
* `性能極高` – 讀的速度是110,000次/s，寫的速度是81,000次/s
* `豐富的數據類型` – 支持二進制的 Strings, Lists, Hashes, Sets 及 Ordered Sets 等數據類型操作
* `原子性` – 所有操作都是原子性的，意思就是要麼成功執行、要麼失敗完全不執行。單個操作是原子性的。多個操作也支持事務，通過`MULTI`和`EXEC`指令包起來。
* 豐富的特性 – 還支持 `publish/subscribe`, 通知, key 過期等等特性

  
數據類型與指令
--------

* Redis支持五種數據類型：String，Hash，List，Set及 Zset (sorted set)
* String (字串):
    * 一個key對應一個value
    * 字串類型是二進制安全的。意思是 redis 的字串可以包含任何數據。比如jpg圖片或者序列化的對象。
    * 設定key，`SET name "redis"`
    * 取key值，`GET name`
    * 刪除特定key，`DEL name`
    * 判斷是否存在key，`EXISTS name`
    * 設定2秒後消失，`EXPIRE name 2`
    * 永久存在，`PERSIST name`
    * 更換key名，`RENAME name newkey`
    * 更換value，`GETSET newkey mongodb`
    * 取得多個key的值，`MGET newkey name`
    * 設定多個key的值，`MSET newkey1 12 newkey2 23`
    * 取得值字數長度，`STRLEN newkey1`
    * 對值加50，`INCRBY newkey1 50`
    * 對值減50，`DECRBY newkey2 50`
    * 對值串字串'test'，`APPEND newkey test`
    * 列出所有key，`KEYS *`
    * 清除所有key，`FLUSHALL`
* Hash (哈希):
    * 一個(Key-Value)對的集合，適合儲存一個VO物件
    * 創建一個物件，`HMSET member name "redis tutorial" description "redis basic commands for caching" likes 20 visitors 23000`
    * 改特定屬性，`HSET member name fabian`
    * 取值，`HGET member name`
    * 取值，`HGET member likes`
    * 取全部資訊，`HGETALL member`
    * 刪除某些屬性（field），`HDEL member description visitors`
    * 是否存在某些屬性，`HEXISTS member description`
    * 對屬性加值，`HINCRBY member likes 50`
    * 獲取該key的所有屬性，`HKEYS member`
    * 獲取該key的所有值，`HVALS member`
    * 該屬性不存在才設值，`HSETNX member name sally`
* List (列表): 
    * 一個有序且可重複的集合
    * 往左插入多個元素，`LPUSH mylist redis mongodb`
    * 往右插入，`RPUSH mylist rabbitmq`
    * 列出第0~10筆，`LRANGE mylist 0 10`
    * 取得index為1的元素，`LINDEX mylist 1`
    * 設定index為1是fabian，`LSET mylist 1 fabian`
    * 取得List長度，`LLEN mylist`
    * 取出並移除最左元素，`LPOP mylist`
    * 取出並移除最右元素，`RPOP mylist`
    * 移出2個元素是fabian，`LREM mylist 2 fabian`
    * 只保留index從1~2的元素，其餘刪除，`LTRIM mylist 1 2`
    * 移出並獲取列表的第一個元素， 如果列表沒有元素會阻塞列表直到等待超時(30秒)或發現可彈出元素為止。`BLPOP mylist mylist2 30`
    * 從列表中彈出一個值，將彈出的元素插入到另外一個列表中並返回它；如果列表沒有元素會阻塞列表直到等待超時(30秒)或發現可彈出元素為止。`BRPOPLPUSH mylist mylist2 30`
* Set (集合):
    * 一個無序且不重複的集合
    * 增加一個或多個，`SADD myset redis mongodb rabbitmq`
    * 列出所有，`SMEMBERS myset`
    * 取得Set長度，`SCARD myset`
    * 隨機取得並移除元素，`SPOP myset`
    * 隨機取得2個元素，`SRANDMEMBER myset 2`
    * 移除某些特定元素，`SREM myset redis rabbitmq`
    * 返回兩個集合的差集，`SDIFF myset myset2`
    * 返回兩個集合的差集並存到另一個集合myset3，`SDIFFSTORE myset3 myset myset2`
    * 返回兩個集合的交集，`SINTER myset myset2`
    * 返回兩個集合的交集並存到另一個集合myset3，`SINTERSTORE myset3 myset myset2`
* Zset (有序集合):
    * 一個有序且不重複的集合
    * 根據傳入的double類型的分數，以小到大排序，分數可以重複
    * 回傳為1代表成功，0代表失敗
    * 增加一個或多個，`ZADD myzset 1 redis 0 mongodb 3 rabitmq 3 rabitmq`
    * 列出第0~1000筆，`ZRANGEBYSCORE myzset 0 1000`
    * 取得Zset長度，`ZCARD myzset`
    * 計算在有序集合中指定區間分數的成員數，`ZCOUNT myzset 1 2`
    * 回傳特定成員的分數，`ZSCORE myzset rabitmq`
    * 對指定成員的分數加4，`ZINCRBY myzset 4 mongodb`
    * 回傳特定成員的索引值(index)，`ZRANK myzset mongodb`
    * 返回所有符合條件 1 < score <= 3 的成員，`ZRANGEBYSCORE myzset (1 3`
    * 移除特定成員，`ZREM myzset mongodb redis`
    * 移除有序集合中給定的排名區間的所有成員，`ZREMRANGEBYRANK myzset 1 2`
    * 移除有序集合中給定的分數區間的所有成員，`	ZREMRANGEBYSCORE myzset 0 2`

進階指令
--------

* Redis事務：
    * 可以一次執行多個命令，並且帶有以下兩個重要的保證：
    * 以`MULTI`命令開始，指令被放入隊列緩存，在發送`EXEC`命令後批次執行。
    * 任務隊列中若有命令執行失敗，其餘的命令依然會被執行，不會rollback。但可以利用`DISCARD`命令自行rollback。
    * 指令範例：
        * `MULTI`
        * `SET book-name "Mastering C++ in 21 days"`
        * `GET book-name`
        * `SADD tag "C++" "Programming" "Mastering Series"`
        * `SMEMBERS tag`
        * `EXEC` or `DISCARD`
    * `Watch` 命令用於監視一個(或多個)key，如果在事務執行之前這個(或這些)key被其他命令所改動，那麼事務將被打斷
    * 指令範例：
        * `WATCH lock lock_times`
        * `MULTI`
        * `SET lock "joe"`  # 就在這時，另一個客戶端修改了 lock_times 的值
        * `INCR lock_times`
        * `EXEC`            # 因為 lock_times 被修改，joe 的事務執行失敗
    * `UNWATCH`: 取消 `WATCH`命令對所有key的監視
    * 指令範例：
        * `UNWATCH`

實際應用
--------

* [參考資料] (https://blog.techbridge.cc/2016/06/18/redis-introduction/)
* 短網址系統
    * 就是一個 hash 對應到一個網址，hash 是用隨機產生，幾碼或是要有什麼符號可以自己決定，接著就把這組對應關係存到資料庫裡面，別人 query 相應的 key 時，你就 redirect 到相應的網址去就好了。這種 key-value 的一對一關係，所以非常適合使用 Redis。
        1. 使用者新增短網址，系統亂數產生 abc123 對應到 http://techbridge.cc
        2. 把 key=abc123, value=http://techbridge.cc 寫進資料庫
        3. 同上，但是是儲存在 Redis，並設定`Expire`的時間為7天
        4. 當有使用者點擊：abc123 這個網址時，先去 Redis 查有沒有這個 key
        5. 有的話，redirect 到對應的網址
        6. 沒有的話，只好去資料庫查，查完之後記得寫一份到 Redis，並設定`Expire`的時間為7天
* 統計系統
    * 造訪次數、圖表、用什麼裝置等等，勢必要記錄每一次 Request，或至少要把 Request 的內容（用什麼手機、時間點、IP）記錄下來，才有數據可以給使用者看。
    * `INCRBY`指令：造訪次數
    * Set (集合) 的使用：不重複的IP
* 高即時性排名系統
    * 中午 12 點開放使用者進入網站，並且回答一題問題
    * 回答完後會看到自己的排名（依答題時間排序），照名次獲得獎品
    * 只有前 300 名有獎品，之後都沒有
    * PS. 由於這個活動只有前 300 名有獎品，預估使用者有 10000 人的話，可能在 10 秒內這個活動就結束了！
        1. 進入網站時，去 Redis 讀取 isOver，查看活動是否結束
        2. 檢查使用者是否答題過，看 Redis 的使用者帳號這個 key 有沒有資料
        3. 若沒答過題且答完題，寫入資料庫，並且把名次寫入 Redis
        4. 若是這個使用者的排名>=300，設定 isOver = true

Java 使用 Redis
--------

* Java Redis Driver: Download and import `jedis-2.9.0.jar` into eclipse (`WebContent\WEB-INF\lib\`) 
* Jedis Connection Pool: Download and import `commons-pool2-2.6.1.jar` into eclipse (`WebContent\WEB-INF\lib\`) 
* Sample code:
    ```java
        JedisPool pool = JedisUtil.getJedisPool();
		Jedis jedis = pool.getResource();
		jedis.auth("123456");
		System.out.println(jedis.ping());

		jedis.close();
		JedisUtil.shutdownJedisPool();
    ˋˋˋ