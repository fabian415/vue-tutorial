Git教學文件
==========

第一次建立專案
------
![Exterior view](../image/git-workflow.png "Git workflow")

* 登入[gitlab網站](https://gitlab.com/)，建立一個新的專案叫vue-tutorial
* 安装Git，輸入命令 `git --version`，測試是否安裝成功？
* `git config --global user.name "Feng-Hsiang Chung"`
* `git config --global user.email "fabian415@gmail.com"`
* Generate SSH keys:
	* 開啟 Git Bash (Windows user)					
	* `ssh-keygen -t rsa -b 4096 -C "fabian415@gmail.com"`
	* passpress: `YOUR_PASSWORD`
	* 產生id_rsa.pub的檔案，ex: ssh-rsa ~一連串金鑰~ jaycelin@HOME-PC
	* 複製 id_rsa.pub 的內容到GitLab網站，Setting，SSH keys
* 移動到專案底下的資料夾，輸入命令 `cd existing_folder`，將專案資料夾建立成 git repository，輸入命令 `git init`
* 手動在專案下建立.gitignore檔，把不要上git的檔案填入
* 輸入命令 `git remote add origin git@gitlab.com:fabian415/vue-tutorial.git`
* 輸入命令 `git add .`
* 輸入命令 `git commit -m "Init project"`
* 輸入命令 `git push -u origin master`

-------

常規運行
------
* 查看分支，輸入命令 `git branch`
* 開出新的分支並切換，輸入命令 `git checkout -b fabian-fix-bugs`
* 開出新的分支fabian-fix-bugs並切換，輸入命令 `git checkout -b fabian-fix-bugs`
* 切到分支fabian-fix-bugs，輸入命令 `git checkout fabian-fix-bugs`
* 刪除分支fabian-fix-bugs，輸入命令 `git branch -D fabian-fix-bugs`
* 從Git repository pull資料下來，輸入命令 `git pull`
* 資料改改改...
* 加入本地暫存區，輸入命令 `git add 檔名` or 加入所有異動的檔案 `git add .`
* commit到本地repository，輸入命令 `git commit -m "寫些commit的內容"`
* push到Git repository，輸入命令 `git push`
* 如果add之後反悔、想將檔案移出的暫存區呢? 輸入命令 
	* `git rm --cached 檔名`
	* `git reset HEAD`
* 如果commit之後反悔，想要將檔案移出？ 輸入命令 `git reset HEAD^`

-------

暫存stash
------
* 輸入命令 `git stash save -u "我是stash暫存"`
* 切到你要改的分支fabian-fix-bugs，輸入命令 `git checkout fabian-fix-bugs`
* 資料改改改...
* 加入本地暫存區，輸入命令 `git add 檔名`
* commit到本地repository，輸入命令 `git commit -m "寫些commit的內容"`
* push到Git repository，輸入命令 `git push`
* 切回你原先的分支fabian-feature，輸入命令 `git checkout fabian-feature`
* 輸入命令 `git stash pop`
* 繼續你的工作囉...

-------

在本地端解決衝突
------
* 若Git上丟一個merge request到dev後，發生conflicts時
* 輸入命令 `git checkout dev`
* 輸入命令 `git pull`
* 輸入命令 `git checkout fabian-fix-bugs`
* 輸入命令 `git merge dev`
* 在VS code裡解決衝突點
* 輸入命令 `git add .`
* 輸入命令 `git commit -m "寫些commit的內容"`
* 輸入命令 `git push`
* 若git上再丟一次merge request到dev

-------

File status tracking
------
![Exterior view](../image/git-file-status.png "File status workflow")

* 四種狀態：
	* untracked (未追蹤的，代表尚未被加入 Git 儲存庫的檔案狀態)
	* unmodified (未修改的，代表檔案第一次被加入，或是檔案內容與 HEAD 內容一致的狀態)
	* modified (已修改的，代表檔案已經被編輯過，或是檔案內容與 HEAD 內容不一致的狀態)
	* staged (等待被 commit 的，代表下次執行 git commit 會將這些檔案全部送入版本庫)
* 若想要追蹤目前檔案的狀態與位置，輸入命令 `git status`
* 以下是執行的結果：
```
On branch master
Changes to be committed:
(use "git reset HEAD <file>..." to unstage)
new file: c.txt
Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git checkout -- <file>..." to discard changes in working directory)
modified: a.txt
Untracked files:
(use "git add <file>..." to include in what will be committed)
b.txt
```

