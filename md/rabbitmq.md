RabbitMQ教學文件
==========

簡介
------
* MQTT是一種基於「發布∕訂閱」機制的訊息傳輸協定（MQTT is a Client Server publish/subscribe messaging transport protocol）

-------

MacOS安裝
------
* Ref: https://andyyou.github.io/2017/06/08/rabbitmq-notes/
* Step 1. 安裝 Homebrew: 
    * `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
* Step 2. 安裝與設定 rabbitmq
    * `brew install rabbitmq`
    * `brew services start rabbitmq`

    
https://hsiaoweiyun.blogspot.com/2018/01/rabbitmq.html