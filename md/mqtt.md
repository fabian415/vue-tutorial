MQTT教學文件
==========

簡介
------
* MQTT是一種基於「發布∕訂閱」機制的訊息傳輸協定（MQTT is a Client Server publish/subscribe messaging transport protocol）

-------

MacOS安裝
------
* Ref: https://oranwind.org/post-post-2/
* Step 1. 安裝 Homebrew: 
    * `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
* Step 2. 安裝與設定 Mosquitto
    * `brew install mosquitto`
    * `brew services list`
    * `brew link mosquitto`
    * `brew services start mosquitto -d`
    * `brew services restart mosquitto`
* Step 3. 測試 Mosquitto MQTT Broker
    * 在一個terminal: `mosquitto_sub -h 127.0.0.1 -t Sensor/Temperature/Room1`
    * 在另一個terminal: `mosquitto_pub -h 127.0.0.1 -t Sensor/Temperature/Room1 -m "hello world"`    